import bge
import mathutils
import math
import random

SPEED = 0.3
SENSITIVITY = 0.5
DEADZONE = 0.015

def init(cont):
	for child in cont.owner.childrenRecursive:
		if isinstance(child, bge.types.KX_Camera):
			cont.owner.scene.active_camera = child
	bge.logic.player = cont.owner
	height = bge.render.getWindowHeight()
	width = bge.render.getWindowWidth()
	bge.render.setMousePosition(int(0.5 * width), int(0.5 * height))
	cont.script = __name__ + '.update'

def update(cont):
	height = bge.render.getWindowHeight()
	width = bge.render.getWindowWidth()
	mouse_pos = - mathutils.Vector([0.5, 0.5]) + mathutils.Vector(bge.logic.mouse.position)
	new_pos = mouse_pos * 0.9 + mathutils.Vector([0.5, 0.5])
	bge.render.setMousePosition(int(new_pos.x * width), int(new_pos.y * height))
	mouse_pos.length = min(mouse_pos.length * SENSITIVITY, SPEED)
	speed = mathutils.Vector([mouse_pos[0], -mouse_pos[1], 0])
	if speed.length > DEADZONE:
		cont.owner.worldPosition += speed

	trail = [obj for obj in cont.owner.scene.objectsInactive if 'Trail' in obj]
	new_trail = cont.owner.scene.addObject(random.choice(trail), cont.owner, 100)
	new_trail.worldPosition.z = -0.5
	new_trail.worldOrientation = [0, 0, random.uniform(-math.pi, math.pi)]
	
	#cont.owner.scene.addObject(cont.owner, 'Trail1')
