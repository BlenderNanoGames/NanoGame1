import bge
import math
import mathutils

GRAVITY = -9.8
SPEED = 5
CLIMB_SPEED = 5
JUMP_FORCE = 100

def init(cont):
	cont.owner['torch'] = [c for c  in cont.owner.childrenRecursive if 'Torch' in c][0]
	cont.script = __name__ + '.run'
	bge.logic.globalDict['level'] = bge.logic.globalDict.get('level', 0) + 1

def sign(num):
	if abs(num) < 0.001:
		return 0
	return num/abs(num)

def run(cont):
	run_from = bge.logic.player
	vec_away = cont.owner.worldPosition - run_from.worldPosition
	direction = sign(vec_away.x)
	distance = vec_away.length
	if direction > 0:
		cont.owner.worldOrientation = [0,math.pi,0]
	else:
		cont.owner.worldOrientation = [0,0,0]
	cont.owner.applyForce([0,GRAVITY, 0])

	cont.owner.localLinearVelocity.x = -SPEED / distance

	front = cont.owner.worldPosition + mathutils.Vector([direction, 0, 0])
	down = cont.owner.worldPosition + mathutils.Vector([0, -1, 0])
	over = cont.owner.worldPosition + mathutils.Vector([0, 0, -1])
	in_front = cont.owner.rayCastTo(front, 0.5) is not None
	on_ground = cont.owner.rayCastTo(down, 0.6) is not None
	is_over = cont.owner.rayCastTo(over, 2)

	if distance < 1.5 and on_ground:
		#print("Here")
		cont.owner.applyForce([0,-GRAVITY + JUMP_FORCE, 0])

	if in_front:
		angle = -math.pi / 6
		cont.owner.worldLinearVelocity.y = CLIMB_SPEED
	else:
		angle = 0
	cont.owner['torch'].localOrientation = [math.pi/2,0,angle + math.pi/2 + (bge.logic.getRandomFloat()-0.3) / distance / 3]

	if is_over is not None and is_over.name == 'Exit':
		path = bge.logic.expandPath("//Level{}.blend".format(bge.logic.globalDict['level'] + 1))
		print(path)
		bge.logic.startGame(path)
		
